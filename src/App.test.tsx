import React from 'react';
import App from './App';
import {Provider} from "react-redux";
import * as ReactDOM from 'react-dom';
import {store} from "./store";

test('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Provider store={store}>
        <App/>
    </Provider>, div);
    ReactDOM.unmountComponentAtNode(div);
});
