import React, {PureComponent} from 'react';
import './App.css';
import {Button, Input, Layout, Menu} from 'antd';
import 'antd/dist/antd.css';
import {ApplicationState, Item} from "./constants/interfaces";
import {connect} from 'react-redux';
import {Dispatch} from "redux";
import {addItem, removeItem, updateItem} from "./actions/treeActions";
import ItemComponent from "./components/ItemComponent";

const {Header, Content, Footer} = Layout;

interface StateProps {
    tree: Item[]
}

interface State {
    startAdding: boolean;
    newItemName: string;
    menuMode: string;
}

interface DispatchProps {
    actions: {
        addItem: (item: Item) => void,
        removeItem: (item: Item) => void
        updateItem: (item: Item) => void;
    }
}

type Props = StateProps & DispatchProps;

class App extends PureComponent<Props, State> {
    public state = {
        startAdding: false,
        newItemName: '',
        menuMode: 'tree'
    };

    createItem = () => {
        const newItem: Item = {id: null, children: [], name: this.state.newItemName, parentId: null};
        this.setState({startAdding: false, newItemName: ''});
        this.props.actions.addItem(newItem);
    };

    render() {
        const {startAdding, menuMode} = this.state;
        const {tree} = this.props;
        return (
            <Layout className="layout" style={{minHeight: "100vh"}}>
                <Header>
                    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1" onClick={() => this.setState({menuMode: 'tree'})}>Tree</Menu.Item>
                        <Menu.Item key="2" onClick={() => this.setState({menuMode: 'json'})}>Json</Menu.Item>
                    </Menu>
                </Header>
                <Content style={{padding: '0 50px'}}>
                    <div className="site-layout-content">
                        {menuMode === 'tree' && <div>
                            {!!tree.length && <div>
                                {tree.map(item => <ItemComponent item={item} addItem={this.props.actions.addItem}
                                                                 removeItem={this.props.actions.removeItem}
                                                                 updateItem={this.props.actions.updateItem}
                                                                 key={item.id || item.name}
                                />)}
                            </div>}
                            {!startAdding &&
                            <Button type="primary" onClick={() => this.setState({startAdding: true})}>
                                Добавить
                            </Button>}
                            {startAdding && <div>
                                <Input.Search size="large" placeholder="Введите имя"
                                              enterButton={<span>Сохранить</span>}
                                              onSearch={this.createItem}
                                              autoFocus
                                              onChange={(event => this.setState({newItemName: event.target.value}))}/>
                            </div>}
                        </div>}
                        {menuMode === 'json' && <pre>{JSON.stringify(tree, undefined, 2)}</pre>}
                    </div>
                </Content>
                <Footer style={{textAlign: 'center'}}>Ant Design</Footer>
            </Layout>
        );
    }
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => {
    return {
        actions: {
            addItem: (item: Item) => dispatch(addItem(item)),
            removeItem: (item: Item) => dispatch(removeItem(item)),
            updateItem: (item: Item) => dispatch(updateItem(item))
        }
    }
};

export default connect((state: ApplicationState) => ({
    tree: state.treeState.tree
}), mapDispatchToProps)(App);
