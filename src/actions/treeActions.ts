import {TreeActionTypes} from "../constants/actionTypes";
import {Item} from "../constants/interfaces";
import {ActionCreator} from "redux";

interface AddItemAction {
    type: TreeActionTypes.ADD_ITEM;
    item: Item;
}

interface RemoveItemAction {
    type: TreeActionTypes.REMOVE_ITEM;
    item: Item;
}

interface UpdateItemAction {
    type: TreeActionTypes.UPDATE_ITEM;
    item: Item;
}

export type treeActions = AddItemAction | RemoveItemAction | UpdateItemAction;

export const addItem: ActionCreator<AddItemAction> = (item: Item) => ({
    type: TreeActionTypes.ADD_ITEM,
    item
});

export const removeItem: ActionCreator<RemoveItemAction> = (item: Item) => ({
    type: TreeActionTypes.REMOVE_ITEM,
    item
});

export const updateItem: ActionCreator<UpdateItemAction> = (item: Item) => ({
    type: TreeActionTypes.UPDATE_ITEM,
    item
});