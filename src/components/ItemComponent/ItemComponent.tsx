import React, {PureComponent} from 'react';
import {Item} from "../../constants/interfaces";
import {Button, Dropdown, Input, Menu} from "antd";
import {DownCircleFilled, UpCircleFilled} from "@ant-design/icons/lib";

interface State {
    startAdding: boolean;
    startEditing: boolean;
    newItemName: string;
    currentItemName: string;
    showChildren: boolean;
}

interface Props {
    item: Item;
    addItem: (item: Item) => void,
    removeItem: (item: Item) => void
    updateItem: (item: Item) => void;
}

class ItemComponent extends PureComponent<Props, State> {
    public state = {
        startAdding: false,
        startEditing: false,
        currentItemName: this.props.item.name,
        newItemName: '',
        showChildren: false,
    };
    deleteItem = (item: Item) => {
        this.props.removeItem(item)
    };
    updateItem = (item: Item) => {
        this.props.updateItem({...item, name: this.state.currentItemName});
        this.setState({startEditing: false})
    };
    createItem = (parentId: string) => {
        const newItem: Item = {id: null, children: [], name: this.state.newItemName, parentId};
        this.setState({startAdding: false, newItemName: '', showChildren: true});
        this.props.addItem(newItem);
    };

    menu = (
        <Menu>
            <Menu.Item onClick={() => this.setState({startAdding: true})}>
                Добавить потомка
            </Menu.Item>
            <Menu.Item onClick={() => this.setState({startEditing: true})}>
                Редактировать
            </Menu.Item>
            <Menu.Item onClick={() => this.props.item.id && this.deleteItem(this.props.item)}>
                Удалить
            </Menu.Item>
        </Menu>
    );

    render() {
        const {item} = this.props;
        const {startEditing, currentItemName, startAdding, showChildren} = this.state;
        return (<div>
            {!startEditing &&
            <div>
                <span>
                     <Dropdown overlay={this.menu} placement="bottomLeft">
                        <Button type="link">{item.name}</Button>
                        </Dropdown>
                </span>
                {!showChildren && !!item.children.length &&
                <Button type="default" shape="circle" onClick={() => this.setState({showChildren: true})}>
                    <DownCircleFilled/>
                </Button>}
                {showChildren &&
                <Button type="default" shape="circle" onClick={() => this.setState({showChildren: false})}>
                    <UpCircleFilled/>
                </Button>}
            </div>}

            {startEditing && <span>
                <Input.Search size="large" placeholder="Введите новое имя"
                              enterButton={<span>Сохранить</span>}
                              onSearch={() => item.id && this.updateItem(item)}
                              onChange={(event => this.setState({currentItemName: event.target.value}))}
                              value={currentItemName}/>
            </span>}
            <div style={{padding: '0 30px'}}>
                {!!item.children.length && showChildren && <div>
                    {item.children.map(nestedItem => <ItemComponent item={nestedItem} addItem={this.props.addItem}
                                                                    removeItem={this.props.removeItem}
                                                                    updateItem={this.props.updateItem}
                                                                    key={nestedItem.id || nestedItem.name}
                    />)}
                </div>}
                {startAdding && <div>
                    <Input.Search size="large" placeholder="Введите имя" enterButton={<span>Сохранить</span>}
                                  onSearch={() => item.id && this.createItem(item.id)}
                                  autoFocus
                                  onChange={(event => this.setState({newItemName: event.target.value}))}/>
                </div>}
            </div>
        </div>)
    }
}

export default ItemComponent;