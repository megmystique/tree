export interface Item {
    name: string;
    parentId: string | null;
    id: string | null;
    children: Item[]
}

export interface ApplicationState {
    treeState: TreeState
}

export interface TreeState {
    tree: Item[]
}