import {Item} from "./constants/interfaces";
import shortid from "shortid";
import {treeActions} from "./actions/treeActions";

export const appendChild = (children: Item[], newItem: Item) => {
    return [...children, {...newItem, id: shortid.generate()}]
};

export const removeChild = (children: Item[], oldItem: Item) => {
    return children.filter(item => item.id !== oldItem.id)
};

export const updateChild = (children: Item[], newItem: Item) => {
    return children.map(el=> el.id===newItem.id? {...el, name: newItem.name}: el)
};

type ActionFunction = typeof appendChild | typeof removeChild | typeof updateChild;

export const searchAndChangeTree = (item: Item, action: treeActions, actionFunction: ActionFunction) => {
    const children = [...item.children];
    if ("item" in action) {
        if (item.id === action.item.parentId) {
            return {
                ...item,
                children: actionFunction(item.children, action.item)
            }
        } else {
            const updatedChildren: Item[] = children.map(el => {
                return searchAndChangeTree(el, action, actionFunction)
            });
            return {...item, children: updatedChildren}
        }
    }
    return item;
};