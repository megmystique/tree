import {TreeActionTypes} from "../constants/actionTypes";
import {TreeState} from '../constants/interfaces';
import {treeActions} from "../actions/treeActions";
import {appendChild, removeChild, searchAndChangeTree, updateChild} from "../helpers";

const initialState: TreeState = {
    tree: []
};

const treeReducer = (state = initialState, action: treeActions) => {
    switch (action.type) {
        case TreeActionTypes.ADD_ITEM:
            return {
                ...state,
                tree: action.item.parentId === null
                    ? appendChild(state.tree, action.item)
                    : state.tree.map(item => searchAndChangeTree(item, action, appendChild))
            };
        case TreeActionTypes.REMOVE_ITEM:
            return {
                ...state,
                tree: action.item.parentId === null
                    ? removeChild(state.tree, action.item)
                    : state.tree.map(item => searchAndChangeTree(item, action, removeChild))
            };
        case TreeActionTypes.UPDATE_ITEM:
            return {
                ...state,
                tree: action.item.parentId === null
                    ? updateChild(state.tree, action.item)
                    : state.tree.map(item => searchAndChangeTree(item, action, updateChild))
            };
        default:
            return state;
    }
};

export default treeReducer;